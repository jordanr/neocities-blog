# This is my little blog 
It's written in [haunt](https://dthompson.us/projects/haunt.html "the haunt project page") and I think it's pretty cool.

## writing
+ Posts tagged with `blog` will be put under /blog.html
+ Posts tagged with `microblog` will be put on the front page.


## building
This project uses [guix](https://guix.gnu.org/ "The gnu package manager") to build itself. If you have guix and make installed running `make` should build the website.

## scripts
The scripts directory contains the [neocli.py](https://github.com/Japoneris/Neo-CLI) script by Japoneris. You'll need to authorize it with your API_KEY in order to use it. Run `./scripts/neocli.py auth` to get started
