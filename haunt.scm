(use-modules (haunt asset)
             (haunt post)
             (haunt page)
             (haunt html)
             (haunt builder blog)
             (haunt builder atom)
             (haunt builder assets)
             (haunt reader)
             (haunt reader commonmark)
             (haunt site)
             (srfi srfi-19)
             (srfi srfi-1)
             (srfi srfi-11))

(define *prefix* "")

(define (orfix prefix . strings)
  (apply string-append (or prefix "") strings))

(define cc-by-sa
  '((p "copyright 2018 - 2024 Jordan Russell") 
    (p (a (@ (rel "license")
             (href "http://creativecommons.org/licenses/by-sa/4.0/"))
          (img (@ (class "inline") (alt "Creative Commons License")
                  (style "border-width:0")
                  (src "https://i.creativecommons.org/l/by-sa/4.0/80x15.png")))))
    (p "This work is licensed under a "
       (a (@ (rel "license")
             (href  "http://creativecommons.org/licenses/by-sa/4.0/"))
          "Creative Commons Attribution-ShareAlike 4.0 International License"))))

(define source-code
  (list '(p "This blog is created with emacs and haunt")
        '(p "You can find the source code " (a (@ (href "https://gitlab.com/jordanr/neocities-blog")) here))))

(define footer
  `(footer ,cc-by-sa ,source-code))

(define (post-header post prefix)
  (define (post-uri post)
    (orfix prefix "/" (post-slug post) ".html"))
  `(div (@ (class "post-header"))
        (h2
         (a (@ (href ,(post-uri post)))
            ,(post-ref post 'title)))
        (time ,(date->string* (post-date post)))))

(define (post-listing post prefix)
  "Make big fancy listing for a post that can be clicked easily on mobile or desktop"
  `(div 
        ,(post-header post prefix)
        (p ,(post-ref post 'summary))))

(define my-blog-theme-vanilla
  (theme #:name "blog theme vanilla"
         #:layout
         (lambda (site title body)
           `((doctype "html")
             (head
              (meta (@ (charset "utf-8")))
              (meta (@ (name "viewport")
                       (conent "width=device-width,initial-scale=1")))
              (link (@ (rel "stylesheet") (type "text/css")
                       (href ,(string-append *prefix* "/assets/css/style.css"))))
              (link (@ (rel "stylesheet") (type "text/css")
                       (href "/assets/css/catppuccin.css")))
              (link (@ (rel "alternate") (type "application/atom+xml")
                       (href "/feed.xml")
                       (title ,(string-append (site-title site) " blog feed"))))
              (title ,(string-append title " - " (site-title site))))
             (body 
              (header (h1 ,(site-title site))
                      (nav 
                       (a (@ (href "/index.html"))
                          "Now")
                       (a (@ (href ,(string-append *prefix* "/blog.html")))
                          "Blog")
                       (a (@ (href ,(string-append *prefix* "/about.html")))
                          "About")))
              (div (@ (id "main"))
                   ,body))))
         #:post-template
         (lambda (post)
           `(article  
             (div (@ (class "post-header")) (h2  ,(post-ref post 'title))
                  (time ,(date->string* (post-date post)))
                  (hr)
                  ,(post-sxml post)
                  (hr)
                  ,footer)))
         #:collection-template
         (lambda (site title posts prefix)
           `(div (@ (id "post-list"))
                 (h2 ,title)
                 (p "("
                    (a (@ 
                        (href ,(orfix prefix "/feed.xml")))
                       "feed")
                    ")")
                 ,@(map (lambda (post)
                          (post-listing post prefix))
                        posts)))))

(define* (microblog #:key
                    (prefix "/")
                    (title "microblog" )
                    (filename "microblog.html")
                    (selector (lambda (x) #t)))
  "A builder for a microblog"
  (lambda (site posts)
    (define (post-template post)
      (post-ref post 'title))
    (define (post-layout post)
      `(li (time ,(date->string (post-ref post 'date) "~B ~d, ~A"))
           (p
            ,(post-sxml post))))
    (define layout (theme-layout my-blog-theme-vanilla))
    (define (collection-template site title posts prefix)
      `(div (@ (id "post-list"))
            (h2 ,title)
            ,@(map (lambda (yearset)
                     (let ((year (car yearset))
                           (posts (cdr yearset)))
                       `(ul (@ (class "timeline"))
                            (h3 ,year)
                            ,@(map post-layout
                                   posts))))
                   (group-posts-by-year (posts/reverse-chronological posts)))))
    (let ((file-name filename)
          (writer sxml->html)
          (contents
           (layout site title
                   (collection-template site
                                        title
                                        (filter selector posts)
                                        prefix))))
      (make-page file-name contents writer))))

(define (group-by pred xs)
  (if (nil? xs)
      '()
      (let-values (((ys zs)
                    (span (lambda (x) (pred (car xs) x)) (cdr xs))))
        (cons (cons (car xs) ys)
              (group-by pred zs)))))

(define* (group-posts-by-year posts)
  (define post-year (compose date-year post-date))
  (define (year=? p1 p2) (= (post-year p1) (post-year p2)))
  (map (lambda (ps) (cons (post-year (car ps)) ps)) (group-by year=? posts)))

(define (misc-post? post)
  (member "misc" (post-ref post 'tags)))

(define (blog-post? post)
  (member "blog" (post-ref post 'tags)))

(define blog-posts/reverse-chronological 
  (compose posts/reverse-chronological
           (lambda (posts)
             (filter blog-post? posts))))

(define (microblog-post? post)
  (member "microblog" (post-ref post 'tags)))

(define microblog/reverse-chronological
  (compose posts/reverse-chronological
           (lambda (posts)
             (filter microblog-post? posts))))

(define mysite
  (site #:title "What I'm up to these days"
        #:domain "what-I-am-upto.neocities.org"
        #:build-directory "what_im_upto"
        #:default-metadata
        '((author . "Jordan Russell")
          (email . "jordan.likes.curry@gmail.com"))
        #:readers (list commonmark-reader sxml-reader html-reader)
        #:builders (list (blog #:theme my-blog-theme-vanilla
                               #:collections
                               `(("Misc Posts" "miscposts.html"
                                  ,(lambda (ps) (filter misc-post? ps)))
                                 ("Recent Posts" "blog.html"
                                  ,blog-posts/reverse-chronological)))
                         (microblog #:filename "index.html"
                                    #:selector microblog-post?
                                    #:title "What I'm doing right this minute")
                         (atom-feed #:filter blog-posts/reverse-chronological)
                         ;;(atom-feeds-by-tag #:blog-prefix *prefix*)
                         (static-directory "assets"))))

mysite
