#!/bin/bash
filename=posts/$(date +%F)-$1_microblogpost.md
cat << EOF > $filename
title: $(date +%F) microblog $1
date: $(date +"%F %H:%M")
tags: microblog
---
EOF

emacsclient +5 -c "$filename" 


