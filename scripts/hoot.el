;;; hoot.el --- A script to capture daily occurences for my blog -*- lexical-binding: t; -*-

;; Copyright (C) 2023 Jordan Russell <jordan.likes.curry@gmail.com

;; Author: Jordan Russell <jordan.likes.curry@gmail.com>
;; Version: 0.1
;; Package-Requires: ((emacs "26.1")
;;                    (with-editor "3.3.2"))
;; Keywords: convenience
;; URL: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This templates a common work flow for my blog, subject to change

;;; Code:

(setq jdr/hoot-posts-directory "~/codes/scheme/guile/neocities-site/posts/")

(defun jdr/hoot (filename)
  (interactive "sWhats happening: ")
  (let ((date-string (format-time-string "%Y-%m-%d %H:%M")))
    (jdr/hoot-setup filename date-string)))

(defun jdr/hoot-setup (filename date-string) 
  (let ((buffer
         (create-file-buffer
          (concat jdr/hoot-posts-directory date-string "-" filename ".scm")))
        (hoot (jdr/hoot-make-hoot filename date-string '())))
    (with-current-buffer buffer
      (insert (jdr/hoot-serialize-hoot hoot))
      (goto-char (point-min))
      (goto-char (word-search-forward ":")))
    (switch-to-buffer-other-window buffer)))

(defun jdr/hoot-make-hoot (title date tags)
  `((title . ,title)
    (date . ,date)
    (tags . ,(append (list 'microblog) tags))
    (summary . ())
    (content . ())))

(defun jdr/hoot-serialize-hoot (hoot)
  (format "%s" hoot))

(provide 'jdr/hoot)
;; hoot.el ends here
