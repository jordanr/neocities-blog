#!/usr/bin/guile -s
!#
(use-modules (srfi srfi-19))

(define* (hoot status #:optional (date (current-date)))
  `((title . ,status)
    (date . ,date)
    (tags . (microblog))
    (summary . ())
    (content . ())))

(let ((arg (cadr (commandline)))
      (date (current-time))
      (port (open-output-file (format "posts/%s"))))
  ())

