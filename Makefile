HAUNT=guix shell -NC -m manifest.scm -- haunt 
all: what_im_upto

what_im_upto: haunt.scm assets/* posts
	${HAUNT} build

.PHONY: serve
serve: what_im_upto
	${HAUNT} serve 


.PHONY: clean
clean:
	rm -rf what_im_upto/

.PHONY: push sync
sync: what_im_upto
	./scripts/neocli.py opti what_im_upto/

push: what_im_upto
	./scripts/neocli.py update what_im_upto/* --remote_path="/" --rec
