title: Adding a microblog
date: 2024-01-12 17:49
summary: I have a cool new microblog
tags: blog
---
I wrote this blog a long time ago. I've done some cool things in the intervening years since I made this blog but I never post about them. I only post about how I make this blog. 

Well starting today I have a very small blog where I can post small updates. Small updates are easier to come up with. They will accumulate over time and it will be cool. I like my microblog.
