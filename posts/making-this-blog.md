title: (re)Making this blog
date: 2021-06-16 16:25
tags: blog
summary: Hello world! (again). Moving my blog to neocities and trying out tailwindcss
---

Hi, thanks for looking at my blog. This blog was started way back in the Summer of 2018, just before I left my little home town and went off to the  University of Washington. Back then this blog was supposed to be about all the cool things I was going to do during my time at the university, but I went and graduated before I really got to post anything.

Today I'm starting my blog again on neocities. There's a few reasons to choose neocities. My university provided students with a little webserver and I used it in a way that you can use also neocities. That is I generated a static site (using [haunt](https://dthompson.us/projects/haunt.html)) and then just uploaded everything to the server, so it's what I'm used to. Another thing is I'm not really a designer and neocities seems like a welcome place to park my funky looking website.

So that's how I came to this little corner of the big wide internet. There's not much to look at right now admittedly. I have a few posts planned, just about some fun stuff I've been up to. Like recording for librivox, mapping my neighborhood with open street maps and hopefully a bit about translating Japanese to English.

__That's it you can go now__

....
But if you're up for it I can talk about tailwindcss
... Alright then here we go.

---
## [Tailwindcss](https://tailwindcss.com/)
The hardest part about making a blog for me is the css, so I thought I'd try out tailwindcss. The selling point with tailwindcss is you can specify your design with a few simple classes on any given element and eventually you get a pretty website at the end. This [blog post](https://adamwathan.me/css-utility-classes-and-separation-of-concerns/) by the creator does a better job explaining this than I ever could.

### Setting up tailwindcss
Tailwindcss is a part of the node ecosystem, and getting things set up with `npm` can be a bit of a headache. Fortunately with a static blog there's a bit less to worry about, but it's still a bit much. To run tailwindcss you'll need [postcss](https://postcss.org/). If tailwindcss is like the engine for your css project, postcss is the car that the engine slots into. (I am told it's more complicated than that, but I'm happy thinking about it like this for now). But wait there's more, without some trimming after compilation, your css file is going to be huge. In order to get it down to a reasonable size you'll need another tool called `purgecss`, which also as it happens can plug into postcss. My css file went from 4.9M to 12K after using purgecss. Oh and I almost forgot, you'll need want something called autoprefixer for browser compatbility.

That's a lot so let's recap. To make your css file you need `postcss` to run `autoprefixer`, `tailwindcss`, and `purgcss`. To make this happen you install all these packages with `npm` and then configure postcss and tailwindcss to actually make these plugins happen. Here's what my postcss configuration file looks like when it's setup to do this.

    // postcss.config.js
    module.exports = {
    plugins: [
        require("tailwindcss"),
        require("autoprefixer"),
      ]
    }

And here's the necessary bit in tailwindcss to make purgecss run

    //tailwindcss.config.js
    module.exports = {
      purge: {
      enabled: true,
        content: ['./what_im_upto/*.html']
      }
    }

But you haven't actually run anything yet. To do that you need to install postcss-cli and run `postcss input.css -o output.css`. This part is actually straight forward.

Every time I use `npm` it feels like there's so many layers upon layers of software at work, it can be kind of daunting. I wonder though if I've just gotten used to the complexity elsewhere and maybe `npm` is just as complex as everything else. Learning C for example I started from the bottom up, learning how to invoke the compiler first, before writing makefiles and then finally wading through autoconf. With `npm` the order is reversed, starting at the most abstract level before moving down to simple commands. But then I remember that `npm` has like a million different build systems and package managers and I realize `npm` is just complex as heck.

### Actually using tailwindcss
Tailwindcss really starts to make sense when you write code that produces html. Since your little html snippets are likely already tightly bound to your css it makes perfect sense that they should be written at the same time. For example I have a function that I use when making a list of posts. It takes a single post and returns a big clickable box that links you to the main post. The function actually outputs a sxml template, which is a way of writing html with s-expressions. If you're not used to sxml just squint at it a bit and you should see the html in it.

Here is the post-listing function.

    (define (post-listing post prefix)
    "Make big fancy listing for a post that can be clicked easily on mobile or desktop"
    `(a  (@ (class "text-black no-underline") (href ,(post-uri post)))
        (div (@ (class "p-2 border-2 m-5 rounded"))
            (h3 (@ (class "mb-0")) ,(post-ref post 'title))
            (date (@ (class "text-xs mb-1 text-gray-500"))
                ,(date->string* (post-date post)))
            (p (@ (class "mt-0 ml-0 mb-1")) ,(post-ref post 'summary)))))

Previously I had a css file to style this particular snippet using custom classes. Now I just write it all at once in a single function and I feel fine about it.

## Conclusion
I like my new blog. It was a bit hard to set up, my css file is bigger than before and it takes 8+ seconds to compile it on my ancient i5-2500k, but now I've got a big box of crayons and they're pretty accessible. Even if I could have done it with normal css, I like learning new things and I think that I will grow into a more proficient css user by using tailwindcss.

---
2021-06-28: This article was edited to show the proper way to set up purgecss with tailwindcss
