title: Edtech sunday
date: 2024-01-28 10:14
tags: microblog
---
+ Read [LARB article on EdTech](https://lareviewofbooks.org/article/inspiration-from-the-luddites-on-brian-merchants-blood-in-the-machine/ "Inspiration from the luddites on brian merchant's blood in the machine")
+ It's interesting the amount of re-evaluation the Luddites are receiving nowadays
+ Read [The linked ACLU report on EdTech](https://www.aclu.org/publications/digital-dystopia-the-danger-in-buying-what-the-edtech-surveillance-industry-is-selling)
+ Also read [The linked techno optimist manifesto](https://a16z.com/the-techno-optimist-manifesto/). It's such a strange rich text; pure unexamined ideology
+ To my (uninformed) mind the ways in which the rich and powerful talk about their machines hasn't changed too much since the 19th century, and if that's the case what exactly has changed about the machines other than degree and implementations.
+ "But the machines don't explain anything, you have to analyze the collective apparatuses of which the machines are just one component." (Gilles Deleuze, Control and Becoming)
